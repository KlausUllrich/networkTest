﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour {

    Transform[] waypoints;
    int currentWaypoint;
    int nextWaypoint;
    float timeMargin;
    float timeSinceLastContact;

    private void Start()
    {
        waypoints = FindObjectOfType<ListOfWaypoints>().wp;
        transform.LookAt(waypoints[0]);
        nextWaypoint = 1;
        timeMargin = 1f; 
    }

    private void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * 3f);
        if (Vector3.Distance(waypoints[currentWaypoint].position, transform.position) < 0.5f && (timeSinceLastContact + timeMargin) < Time.timeSinceLevelLoad )
        {
            currentWaypoint = nextWaypoint;
            nextWaypoint++;
            if (nextWaypoint >= waypoints.Length)
                nextWaypoint = 0;
            transform.LookAt(waypoints[currentWaypoint]); 
            timeSinceLastContact = Time.timeSinceLevelLoad;
        }
    }
}
