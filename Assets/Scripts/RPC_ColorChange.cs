﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class RPC_ColorChange : NetworkBehaviour {

    public Material[] material;
    int curColOfThisObject;
    [SyncVar (hook="OnSyncVar")]
    int curSyncVarOfThisObject;

    Text text;

    private void Start()
    {
        text = GetComponentInChildren<Text>();
    }

    [Server]
    public void SyncColorVar()
    {
        Debug.Log("------------ SyncColorVar() before changing the SyncVar");
        // do the calculation only on the server
        curSyncVarOfThisObject = curColOfThisObject;

        Debug.Log("------------ SyncColorVar() done");
    }


    public int GetCurColor()
    {
        return curColOfThisObject;
    }


    [ClientRpc]
    public void RpcNextColor(int newValue)
    {
        Debug.Log("------------ RpcNextColor()");
        if (!isClient)
            return;

        // check for discrepancies on the clients
        if (newValue >= material.Length)
        {
            Debug.LogError("Client object does not have such a material available");
            return;
        }

        // update the curCol variable on the clients
        actualChangeColorCode();
   }


    void actualChangeColorCode ()
    {
        Debug.Log("------------ actualChangeColorCode()");

        curColOfThisObject++;
        if (curColOfThisObject >= material.Length)
            curColOfThisObject = 0;

        Debug.Log("curCol: " + curColOfThisObject + "     SyncVar: " + curSyncVarOfThisObject);

        // set the material on the client
        this.GetComponent<MeshRenderer>().material = material[curColOfThisObject];

    }

    private void OnSyncVar (int newValue)
    {
        Debug.Log("------------ OnSyncVar()");
        Debug.Log("OnSyncVar newValue: " + newValue + "     curCol: " + curColOfThisObject + "     SyncVar: " + curSyncVarOfThisObject);
    }

    private void Update()
    {
        if (isClient)
        {
            text.text = "new color of this object: " + curColOfThisObject.ToString() +
                        "\nnew SyncVar of this object: " + curSyncVarOfThisObject;
        }
    }

}
