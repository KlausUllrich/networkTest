﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerEventManager : NetworkBehaviour
{
    public GameObject capsule;

    Camera cam;
    Transform startingPosition;

    private void Start()
    {
        startingPosition = FindObjectOfType<ListOfWaypoints>().wp[0];
        cam = FindObjectOfType<Camera>();
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        #region MoveCamera
        cam.transform.tag = "MainCamera";

        float x = Input.GetAxis("Horizontal") * Time.deltaTime * 50f;
        float z = Input.GetAxis("Vertical") * Time.deltaTime * 40f;

        cam.transform.Translate(x, 0, z, Space.World);
        #endregion

        if (Input.GetKeyUp(KeyCode.Space))
        {
            CmdSpawnCapsule();
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.GetComponent<RPC_ColorChange>() != null)
                {
                    // send through network only when necessary
                    CmdNextColor(hit.transform.gameObject);
                } 
            }
        } 
    }

    [Command]
    private void CmdSpawnCapsule()
    {
        GameObject spawnObject = Instantiate(capsule, startingPosition.position, Quaternion.identity);
        NetworkServer.Spawn(spawnObject);
    }

    [Command]
    public void CmdNextColor(GameObject hitObject)
    {
        RPC_ColorChange colorChange = hitObject.GetComponent<RPC_ColorChange>();
        if (colorChange != null)
        {
            int curColor = colorChange.GetCurColor();

            Debug.Log("*** Capsule clicked ***");

            // change it on the clients
            Debug.Log("------------ PlayerEventManager: Call RpcNextColor()");
            colorChange.RpcNextColor(curColor);

            // change the SyncVar
            Debug.Log("------------ PlayerEventManager: Call SyncColorVar()");
            colorChange.SyncColorVar();
        }
    }
}
